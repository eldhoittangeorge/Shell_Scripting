#This program is to demonstrate the use of if,elif and else
echo "enter a number between 10 and 20:"
read num
if [ $num -gt 20 ]
then 
echo "number is greater than 20"
elif [ $num -lt 10 ]
then 
echo "the number is less than 10"
else
echo "you have enterd the correct number"
fi
